# **Mapas Conceptuales**
## 1. La consultoría
```plantuml
@startmindmap
*[#BEAEE2] **La consultoría**
**_ **Que Es?**
***[#F7DBF0] Empresa de servicios profesionales\nde alto valor añadido.
****_ **Tiene**
*****[#B5EAEA] Conocimiento sectorial
*****[#B5EAEA] Conocimiento Técnico
*****[#B5EAEA] Capacidad de plantear soluciones

**_ **Que Servicios Presta?**
***[#BEAEE2] **consultoría**
****_ **Servicios**
*****[#F7DBF0] Reingeniería de procesos\nGobierno TI.\nOficina De Proyectos\nAnalitica avanzada de datos
******_ **Social**
*******[#CDF0EA] SentimentalAnalysis
*******[#CDF0EA] Definición de mejoras de utilización de\ncanales de atención por redes sociales
******_ **Movilidad**
*******[#CDF0EA] Definición de prácticas de Gobierno TI\npara Mobile
*******[#CDF0EA] Simplificación de procesos de movilidad
******_ **Analytics**
*******[#CDF0EA] Definición de arquitectura BIGDATA
*******[#CDF0EA] Desarrollo de modelos analíticos avanzados
******_ **Cloud**
*******[#CDF0EA] Definición de Estrategia Cloud
*******[#CDF0EA] Definición de Procedimientos de Operación Cloud

***[#CDF0EA] **Integración**
****_ **Servicios**
*****[#F38BA0] Desarrollos a medida\nAseguramiento de la calidad 2E2\nInfraestructura\nSoluciones de mercado
******_  **Social**
*******[#F7DBF0] Desarrollo de conectores con redes sociales
******_ **Movilidad**
*******[#F7DBF0] Movilización de aplicaciones existentes
*******[#F7DBF0] Desarrollo y prueba de apps
*******[#F7DBF0] Integración de soluciones
******_ **Analytics**
*******[#F7DBF0] Integración de fuentes
*******[#F7DBF0] Desarrollo de CDM
*******[#F7DBF0] Integración de entornos relacionales y BIGDATA
******_ **Cloud**
*******[#F7DBF0] Implantación de estrategia CLOUD
*******[#F7DBF0] Desarrollo de Herramientas de Provisión CLOUD
*******[#F7DBF0] Desarrollo de aplicaciones CLOUD

***[#F38BA0] **Externalización**
****_ **Servicios**
*****[#CDF0EA] Gestión de aplicaciones\nServicios SQA\nOperación y Adm. de infra\nProcesos de negocio
******_ **Social**
*******[#BEAEE2] Operación Comunity Manager
******_ **Movilidad**
*******[#BEAEE2] Operación de plataformas de\n aplicaciones móviles
******_ **Analytics**
*******[#BEAEE2] Factoría de explotación y mantenimiento de modelos analíticos
******_ **Cloud**
*******[#BEAEE2] operación de servicios Cloud

**_ **Por Que Es Una Profesión De Futuro?**
***[#B3E283] En la sociedad del conocimiento, la consultoría,\n es un modo de vida, que permite\n alcanzar la felicidad profesional
****[#F3C583] La consultoría tiene un papel clave como proveedor\nde servicios de habilitación digital.
****_ **Iniciativas De Futuro**
*****[#E8E46E] Transformación digital
*****[#E8E46E] Industria Conectad 4.0
*****[#E8E46E] Smart Cities

**_ **Que Exige La consultoría?**
***[#CDF0EA] Aptitud
****_ **Por Su**
*****[#F6C6EA] Formación profesional
*****[#F6C6EA] Capacidad de trabajo y evolución
****_ **Capaces De**
*****[#F6C6EA] Colaborar en un proyecto común
*****[#F6C6EA] Trabajar en equipo
*****[#F6C6EA] Ofrecer honestidad y sinceridad
***[#CDF0EA] Actitud
****[#F6C6EA] Proactividad
****[#F6C6EA] Voluntad de mejora
****[#F6C6EA] Responsabilidad
***[#CDF0EA] Compromiso
****[#F6C6EA] Disponibilidad Total
****[#F6C6EA] Nuevos Retos
****[#F6C6EA] Viajes

**_ **Como Es La Carrera Profesional En consultoría?**
***[#C449C2] Modelo de RRHH
****_ **Tiene**
*****[#867AE9] Modelo de categorías
*****[#867AE9] Plan de carrera
*****[#867AE9] Proceso de evaluación 
*****[#867AE9] Plan de formación
***[#C449C2] consultoría
****_ **Niveles**
*****[#A6D6D6] Junior
******_ **Se Divide En**
*******[#FFC288] Asistente
*******[#FFC288] Consultor Junior
*******[#FFC288] Consultor
*****[#A6D6D6] Senior
******_ **Se Divide En**
*******[#FFC288] Consultor Senior
*******[#FFC288] Jefe De Equipo
*******[#FFC288] Jefe De Proyecto
*****[#A6D6D6] Gerente
*****[#A6D6D6] Director

@endmindmap
```

## 2. Consultoría de Software
```plantuml
@startmindmap
* **Consultoría de Software**

** **Desarrollo De Software**
***_ **Proceso**
**** Comienza cuando un cliente tiene una\nnecesidad dentro de su negocio.
***** Consultoría
******_ **Hablar con el cliente**
******* Ver requisitos o requerimientos tiene ese\software que se quiere crear.
******_ **Realizar un estudio de viabilidad**
******* Analizar lo que él quiere.
******* Ventajas o que ahorro económico va a\ntener con ese software.
******* Mirar que coste va a llevar hacer ese desarrollo
******* Estudiar con el cliente si es viable o no.
******_ **Diseño Funcional**
******* Ver que información necesita ese software\nde entrada/salida en el sistema
******* Que modelo de datos vamos a tener
******* Cuantos procesos repetitivos va a tener ese sistema
******* Se Crea un prototipo como muestra para el cliente
******_ **Diseño técnico**
******* Se traslada la necesidad a un lenguaje de programación
******* Crear procesos y necesidades de datos 
******* Realizar las pruebas funcionales del software
***_ **Como Se Desarrolla**
**** Jefe de proyecto
***** Planifica el proyecto 
***** Ve que recursos se necesitan
***** Habla con el cliente

** **Actividades Desarrolladas**
*** Se realizan las tareas que permiten crear o \nmantener la informática de una empresa
****_ **Que necesita una empresa?**
***** Crear infraestructura
****** Ordenadores
****** Servidores
****** infraestructura De Comunicaciones
****** Mantener segura la información
****** Aplicaciones para desarrollar su negocio
****_ **Como Hacer Que Funcione?**
***** Estar constantemente monitorizando \nlos procesos que se están lanzando
***** Estar siempre pendiente de que la infraestructura\n funcione correctamente
***** Adecuar las aplicaciones a los cambios\n para no tener fallos.
****_ **Como Influye El Factor De Escala?**
***** Grandes Clientes
******_ **Se Atienden**
******* Oficinas de proyecto que atienden a grandes clientes, ellos tienen muchos proyectos y servicios
******* Equipos especializados en ese cliente
******* Conocer el sector del cliente
***** Clientes Pequeños
******_ **Se Atienden**
******* De una forma más personalizada hablar con el cliente
******* Equipos pequeños para cubrir sus necesidades
******* Servicio más general

** **Desarrollo De Servicios**
***_ **Tendencias**
**** Almacenar la información en la nube
**** Cambiar la forma de tratar la \ninformacion a través de BIGDATA
**** Automatizar la operación del día a día
***_ **Inconvenientes**
**** Información en manos de un tercero (Nube)
**** Costo de los servicios en la Nube
**** La infraestructura no depende de ti

** **Visita Técnica A AXPE**
*** AXPE Consulting Nacida en Madrid, presta servicios en \n4 países directamente y 20 de modo indirecto.
****_ **Servicios**
***** Consultoría
***** Desarrollo de software
****_ **Futuro**
***** Esta enfocado al desarrollo de servicios de alto valor
***** Extensión Geográfica

@endmindmap
```
## 3. Aplicación de la ingeniería de software
```plantuml
@startmindmap
* **Aplicación de la ingeniería de software**

** Stratesys
***_ **Que es?**
**** Es una empresa de servicios de tecnología \nque lleva en el mercado unos 15 años, \nexpertos en soluciones app. 

*****_ **Enfoque Metodológico**
****** Recoger los requisitos del cliente
****** Reunirse con todas las áreas de la empresa
****** Identificar los procesos que esa empresa ejecuta en su día a día
****** Se genera un análisis funcional

*****_ **Análisis técnico**
****** Analizar cada una de las cosas que se van a hacer,\n como se realizan a nivel técnico.

*****_ **Fases**
****** Análisis
******* Saber que es lo queremos
****** Construcción
******* Prototipo de lo que se está construyendo 
****** Pruebas
******* Pruebas del usuario documentadas

*****_ **Herramientas**
****** Bibliotecas De Template
****** Herramientas de procesos
****** Reuniones semanales

*****_ **Diseño técnico**
****** Consultores Funcionales
******* Se dedican a configurar la herramienta.
****** Programación 
******* Cada requisito funcional debe tener diseños técnicos
@endmindmap
```
